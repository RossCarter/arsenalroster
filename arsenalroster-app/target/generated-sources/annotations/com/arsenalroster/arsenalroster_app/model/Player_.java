package com.arsenalroster.arsenalroster_app.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Player.class)
public abstract class Player_ {

	public static volatile SingularAttribute<Player, Integer> Goals;
	public static volatile SingularAttribute<Player, Integer> SquadNumber;
	public static volatile SingularAttribute<Player, String> PlayingPosition;
	public static volatile SingularAttribute<Player, Integer> Age;
	public static volatile SingularAttribute<Player, Integer> Assists;
	public static volatile SingularAttribute<Player, Integer> ID;
	public static volatile SingularAttribute<Player, String> FirstName;
	public static volatile SingularAttribute<Player, Integer> Appearances;
	public static volatile SingularAttribute<Player, String> LastName;
	public static volatile SingularAttribute<Player, String> Nationality;

}


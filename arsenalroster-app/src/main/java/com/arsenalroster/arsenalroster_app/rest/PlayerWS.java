package com.arsenalroster.arsenalroster_app.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.arsenalroster.arsenalroster_app.data.PlayerDAO;
import com.arsenalroster.arsenalroster_app.model.Player;

@Path("/players")
@Stateless
@LocalBean
public class PlayerWS {
	
	@EJB
	private PlayerDAO playerDAO;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPlayers() {
		List<Player> players = playerDAO.getAllPlayers();
		return Response.status(200).entity(players).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getByNationality/{nationality}")
	public Response getPlayersByNationality(@PathParam("nationality") String nationality) {
		List<Player> players = playerDAO.getPlayersByNationality(nationality);
		return Response.status(200).entity(players).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getByPosition/{position}")
	public Response getPlayersByPosition(@PathParam("position") String position) {
		List<Player> players = playerDAO.getPlayersByPosition(position);
		return Response.status(200).entity(players).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getByID/{id}")
	public Response getPlayerByID(@PathParam("id") int id) {
		Player player = playerDAO.getPlayerByID(id);
		return Response.status(200).entity(player).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getByFirstName/{firstname}")
	public Response getPlayerByFirstName(@PathParam("firstname") String firstname) {
		List<Player> players = playerDAO.getPlayersByFirstName(firstname);
		return Response.status(200).entity(players).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getByLastName/{lastname}")
	public Response getPlayerByLastName(@PathParam("lastname") String lastname) {
		List<Player> players = playerDAO.getPlayersByLastName(lastname);
		return Response.status(200).entity(players).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getByGoals/{goals}")
	public Response getPlayersByGoals(@PathParam("goals") int goals) {
		List<Player> players = playerDAO.getPlayersByGoals(goals);
		return Response.status(200).entity(players).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getByApps/{apps}")
	public Response getPlayersByAppearances(@PathParam("apps") int appearances) {
		List<Player> players = playerDAO.getPlayersByAppearances(appearances);
		return Response.status(200).entity(players).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBySquadNumber/{squadNum}")
	public Response getPlayerBySquadNumber(@PathParam("squadNum") int squadNum) {
		List<Player> players = playerDAO.getPlayerBySquadNumber(squadNum);
		return Response.status(200).entity(players).build();
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response addPlayer(Player player) {
		playerDAO.addPlayer(player);
		return Response.status(201).entity(player).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updatePlayer(Player player) {
		playerDAO.updatePlayer(player);
		return Response.status(200).entity(player).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deletePlayer(@PathParam("id") int id) {
		playerDAO.deletePlayer(id);
		return Response.status(204).build();
	}
}

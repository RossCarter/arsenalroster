package com.arsenalroster.arsenalroster_app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Player {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int ID;
	
	private String FirstName;
	private String LastName;
	private int Age;
	private String Nationality;
	private String PlayingPosition;
	private int SquadNumber;
	private int Goals;
	private int Assists;
	private int Appearances;
	private String image;
	
	public int getId() {
		return ID;
	}
	
	public void setID(int iD) {
		ID = iD;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		Age = age;
	}
	public String getNationality() {
		return Nationality;
	}
	public void setNationality(String nationality) {
		Nationality = nationality;
	}
	public String getPlayingPosition() {
		return PlayingPosition;
	}
	public void setPlayingPosition(String playingPosition) {
		PlayingPosition = playingPosition;
	}
	public int getSquadNumber() {
		return SquadNumber;
	}
	public void setSquadNumber(int squadNumber) {
		SquadNumber = squadNumber;
	}
	public int getGoals() {
		return Goals;
	}
	public void setGoals(int goals) {
		Goals = goals;
	}
	public int getAssists() {
		return Assists;
	}
	public void setAssists(int assists) {
		Assists = assists;
	}
	public int getAppearances() {
		return Appearances;
	}
	public void setAppearances(int appearances) {
		Appearances = appearances;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}

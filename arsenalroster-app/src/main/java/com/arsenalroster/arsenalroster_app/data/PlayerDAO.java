package com.arsenalroster.arsenalroster_app.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.arsenalroster.arsenalroster_app.model.Player;

@Stateless
@LocalBean
public class PlayerDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<Player> getAllPlayers(){
		Query query = entityManager.createQuery("Select p FROM Player p");
		return query.getResultList();
	} 
	
	public List<Player> getPlayersByNationality(String nationality){
		Query query = entityManager.createQuery("SELECT p FROM Player AS p" + " WHERE p.Nationality = :nationality");
		query.setParameter("nationality", nationality);
		return query.getResultList();
	}
	
	public List<Player> getPlayersByPosition(String playingposition){
		Query query = entityManager.createQuery("SELECT p FROM Player AS p" + " WHERE p.PlayingPosition = :PlayingPosition");
		query.setParameter("PlayingPosition", playingposition);
		return query.getResultList();
	}
	
	public Player getPlayerByID(int player_ID) {
		return entityManager.find(Player.class, player_ID);
	}
	
	public List<Player> getPlayerBySquadNumber(int squadNumber) {
		Query query = entityManager.createQuery("SELECT p FROM Player AS p" + " WHERE p.SquadNumber = :squadNum");
		query.setParameter("squadNum", squadNumber);
		return (List<Player>) query.getResultList();
	}
	
	public List<Player> getPlayersByFirstName(String firstname) {
		Query query = entityManager.createQuery("SELECT p FROM Player AS p" + " WHERE p.FirstName = :FirstName");
		query.setParameter("FirstName", firstname);
		return (List<Player>) query.getResultList();
	}
	
	public List<Player> getPlayersByLastName(String lastname) {
		Query query = entityManager.createQuery("SELECT p FROM Player AS p" + " WHERE p.LastName = :LastName");
		query.setParameter("LastName", lastname);
		return (List<Player>) query.getResultList();
	}
	
	public List<Player> getPlayersByGoals(int goalsLimit){
		Query query = entityManager.createQuery("SELECT p FROM Player AS p" + " WHERE p.Goals >= :goals");
		query.setParameter("goals", goalsLimit);
		return (List<Player>) query.getResultList();
	}
	
	public List<Player> getPlayersByAppearances(int appearancesLimit){
		Query query = entityManager.createQuery("SELECT p FROM Player AS p" + " WHERE p.Appearances >= :apps");
		query.setParameter("apps", appearancesLimit);
		return (List<Player>) query.getResultList();
	}
	
	public void addPlayer(Player player) {
		entityManager.persist(player);
	}
	
	public void updatePlayer(Player player) {
		entityManager.merge(player);
	}
	
	public void deletePlayer(int id) {
		entityManager.remove(getPlayerByID(id));
	}
}

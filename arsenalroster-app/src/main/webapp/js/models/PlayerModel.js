var rootURL = "http://localhost:8080/arsenalroster-app/arsenal/players";

var Player = Backbone.Model.extend({
		urlRoot:rootURL,
		defaults:{
			"id":null
		},
		initialize: function(){
		}
});

var PlayerList = Backbone.Collection.extend({
	model: Player,
	url:rootURL
});

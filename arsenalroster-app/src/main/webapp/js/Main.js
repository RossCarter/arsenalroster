var mainView;
var playerList;
//The rendered main view is put on the page.
$(document).ready(function(){
	//console.log("here");
	playerList = new PlayerList();
	playerList.fetch({
		success: function(data){
			mainView = new MainView({collection: playerList});
			mainView.$el.appendTo(document.body);
		}
	});
});
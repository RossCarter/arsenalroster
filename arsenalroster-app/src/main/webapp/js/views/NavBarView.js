NavBarView = Backbone.View.extend({
	render: function(){
		var template= _.template($('#navTemplate').html());
		return this.$el.html(template);
	}
})
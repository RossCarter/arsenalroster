PlayerView = Backbone.View.extend({
	model: Player,
	tagName:'tr',
	events:{
		  "click #editPlayerMain": "showEditPlayerModal"
	},
	showEditPlayerModal: function(e){
		  var player=this.model;
		  $('#MainArea').html(new PlayerDetailsView({model:player}).render());
		  $('#editPlayerModal').modal('show');
	},
	render: function(){
		var template= _.template($('#player_list').html(), this.model.toJSON());
		return this.$el.html(template);
	}
})
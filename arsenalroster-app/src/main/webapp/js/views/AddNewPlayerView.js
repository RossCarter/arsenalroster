AddNewPlayerView = Backbone.View.extend({
	model: Player,
	id: "addContainer",
	initialize: function(){
	},
	events:{
		  "click #addPlayerButton": "addNewPlayer",
		  "click #addNewPlayer": "showAddPlayerModal"
	},
	addNewPlayer: function(e){	
		var playerDetails= {id:$('#idEdit').val(),
		  		  squadNumber:$('#squadNumber').val(),
				  firstName:$('#firstName').val(),
				  lastName:$('#lastName').val(),
				  age:$('#age').val(),
				  playingPosition:$('#position').val() ,   
				  nationality:$('#nationality').val(),
				  goals:$('#goals').val(),
				  assists:$('#assists').val(),
				  appearances:$('#appearances').val(),
				  image:$('#image').val() };
		
			if(this.isInputFieldsForAddPlayerValid()){
				 this.showSuccessMessage('Player has been added successfully!');
				this.model.save({
					squadNumber:$('#squadNumber').val(),
					firstName:$('#firstName').val(),
					lastName:$('#lastName').val(),
					age:$('#age').val(),
					playingPosition:$('#position').val() ,   
					nationality:$('#nationality').val(),
					goals:$('#goals').val(),
					assists:$('#assists').val(),
			  	appearances:$('#appearances').val(),
			  	image:$('#image').val() 
				},{
				  success:function(wine){
					  this.model= new Player(playerDetails);
					  playerList.add(this.model);
					  mainView.renderList();
					  $('#playerRegistrationForm').modal('hide');
				  }
			  });
		}else{
			console.log("Error");
		}
		  return false;
	},
	showAddPlayerModal: function(e){
		  $('#playerRegistrationForm').modal('show');
	},
	isInputFieldsForAddPlayerValid: function() {
		var squadNumber = $('#squadNumber').val();
		var firstName = $('#firstName').val();
		var lastName = $('#lastName').val();
		var age = $('#age').val();
		var position = $('#Position').val();
		var nationality = $('#Nationality').val();
		var goals = $('#Goals').val();
		var assists = $('#Assists').val();
		var appearances = $('#Appearances').val();
		var image = $('#Image').val();
		
		console.log(squadNumber);
		if (squadNumber == '') {
			this.showErrorMessage("Please enter a Squad Number");
			return false;
		} else if (firstName == '') {
			this.showErrorMessage("Please enter a first name");
			return false;
		} else if (lastName == '') {
			this.showErrorMessage("Please enter an last name");
			return false;
		} else if (age == 0) {
			this.showErrorMessage("Please enter an age");
			return false;
		}else if (position == '') {
			this.showErrorMessage("Please enter a position");
			return false;
		}else if (nationality == '') {
			this.showErrorMessage("Please enter a nationality");
			return false;
		}else if (image == '') {
			this.showErrorMessage("Please enter an image");
			return false;
		}else {
			return true;
		}
	},
	showErrorMessage:function(message) {
		toastr.options = {
			"debug" : false,
			"positionClass" : "toast-top-center",
			"onclick" : null,
			"fadeIn" : 300,
			"fadeOut" : false,
			"progressBar" : true,
			"timeOut" : 7000,
			"closeButton" : true,
			"extendedTimeOut" : 10000,
			"preventDuplicates" : true
		}
		toastr.error(message);
	},
	showSuccessMessage:function(message) {
		toastr.options = {
			"debug" : false,
			"positionClass" : "toast-top-center",
			"onclick" : null,
			"fadeIn" : 300,
			"progressBar" : true,
			"fadeOut" : 1000,
			"timeOut" : 7000,
			"closeButton" : true,
			"extendedTimeOut" : 10000,
			"preventDuplicates" : true
		}
		toastr.success(message);
	},
	render: function(){
		var template= _.template($('#playerRegistrationTemplate').html());
		return this.$el.html(template);
	},
})
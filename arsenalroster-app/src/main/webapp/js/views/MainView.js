
var MainView = Backbone.View.extend({
  collection: PlayerList,
  id: "container",
  initialize: function(){
   this.listenTo(this.collection, 'add', this.renderList);
   this.render();
  },

  render: function(){
	  $('#players').append(new PlayerGridView({collection:this.collection}).render());
	  $('#NavDiv').html(new NavBarView({}).render()); 
	 $('#TableDiv').html(new PlayerTableView({}).render()); 
    this.collection.each(function(player){
    $('#playerTableBody').append(new PlayerView({model:player}).render());
    }, this);
    var player = new Player();
    $('#addButtonDiv').html(new AddNewPlayerView({model: player}).render());
    $('#playerManagementTable').DataTable(); 
  },
  
  renderList:function(){
	  $('#playerManagementTable').dataTable().fnDestroy();
      $('#playerManagementTable tbody').empty();
	  $('#playerTableBody tr').remove();
	  this.collection.each(function(player){
		    $('#playerTableBody').append(new PlayerView({model:player}).render());
		    }, this);
	  $('#playerManagementTable').DataTable(); 
  }
});
PlayerTableView = Backbone.View.extend({
	render: function(){
		var template= _.template($('#playerTable').html());
		return this.$el.html(template);
	}
})
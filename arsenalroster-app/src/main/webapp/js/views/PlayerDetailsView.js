var PlayerDetailsView = Backbone.View.extend({
	model: Player,
	id: "editContainer",
	events:{
		  "click #savePlayer": "savePlayer",
		  "click #deletePlayer": "deletePlayer",
		  "click #addNewPlayerInModal": "addPlayer",
		  "click #addPlayerButton": "addNewPlayer",
	  },
	  savePlayer: function(e){
		  var playerDetails= {
				  		  squadNumber:$('#squadNumberEdit').val(),
						  firstName:$('#firstNameEdit').val(),
						  lastName:$('#lastNameEdit').val(),
						  age:$('#ageEdit').val(),
						  playingPosition:$('#positionEdit').val() ,   
						  nationality:$('#editNationality').val(),
						  goals:$('#editGoals').val(),
						  assists:$('#editAssists').val(),
						  appearances:$('#editAppearances').val(),
						  image:$('#editImage').val() };
		
		  if(this.isInputFieldsForEditPlayerValid()){
		  if (($('#idEdit').val()) == ''){
			  this.model= new Player(playerDetails);
			  this.showSuccessMessage('Player has been <strong>added</strong> to the roster!');
			  playerList.add(this.model);
			  this.model.save({
		  		  squadNumber:$('#squadNumberEdit').val(),
				  firstName:$('#firstNameEdit').val(),
				  lastName:$('#lastNameEdit').val(),
				  age:$('#ageEdit').val(),
				  playingPosition:$('#positionEdit').val() ,   
				  nationality:$('#editNationality').val(),
				  goals:$('#editGoals').val(),
				  assists:$('#editAssists').val(),
				  appearances:$('#editAppearances').val(),
				  image:$('#editImage').val() },{
					  success:function(player){
							  mainView.renderList();
							  $('#editPlayerModal').modal('hide');
							  $('#playerManagementTable').DataTable(); 
						  }
					  });
		} else {
			this.showSuccessMessage('Player Details have been updated!');
		  this.model.save({
			  id:$('#idEdit').val(),
	  		  squadNumber:$('#squadNumberEdit').val(),
			  firstName:$('#firstNameEdit').val(),
			  lastName:$('#lastNameEdit').val(),
			  age:$('#ageEdit').val(),
			  playingPosition:$('#positionEdit').val() ,   
			  nationality:$('#editNationality').val(),
			  goals:$('#editGoals').val(),
			  assists:$('#editAssists').val(),
			  appearances:$('#editAppearances').val(),
			  image:$('#editImage').val() 
			  },{
				  success:function(wine){
					  mainView.renderList();
					  $('#editPlayerModal').modal('hide');
				  }
			  });
		  }
		  }
		  else{
			  console.log("Error in editAdd");
		  }
		  return false;
	  },
	  deletePlayer: function(e){
		  var playerId= parseInt($('#idEdit').val());
		  this.showErrorMessage('Player has removed from the roster!');
		  this.model.set({id:playerId});
		  this.model.destroy(
				  {success:function(data){
					  mainView.renderList();
					  $('#editPlayerModal').modal('hide');
				  }
		  });
		  return false;
	  },
	  addPlayer: function(e){
		  $('#deletePlayer').hide();
		  $('#idEdit').val(null);
		  $('#squadNumberEdit').val("");  
		  $('#firstNameEdit').val("");
		  $('#lastNameEdit').val("");
		  $('#ageEdit').val(""); 
		  $('#positionEdit').val("");     
		  $('#editNationality').val("") ;
		  $('#editGoals').val("") ;
		  $('#editAssists').val("") ;
		  $('#editAppearances').val("") ;
		  $('#editImage').val("") ;
		  return false;
	  },
	  addNewPlayer: function(e){
		  $('#editPlayerModal').modal('show');
		  $('#deletePlayer').hide();
		  $('#idEdit').val(null);
		  $('#squadNumberEdit').val("");  
		  $('#firstNameEdit').val("");
		  $('#lastNameEdit').val("");
		  $('#ageEdit').val(""); 
		  $('#positionEdit').val("");     
		  $('#editNationality').val("") ;
		  $('#editGoals').val("") ;
		  $('#editAssists').val("") ;
		  $('#editAppearances').val("") ;
		  $('#editImage').val("") ;
		  return false;
	  },
	  isInputFieldsForEditPlayerValid: function(e) {
			var squadNumber = $('#squadNumberEdit').val();
			var firstName = $('#firstNameEdit').val();
			var lastName = $('#lastNameEdit').val();
			var age = $('#ageEdit').val();
			var position = $('#editPosition').val();
			var nationality = $('#editNationality').val();
			var goals = $('#editGoals').val();
			var assists = $('#editAssists').val();
			var appearances = $('#editAppearances').val();
			var image = $('#editImage').val();
		
			if (squadNumber == '') {
				this.showErrorMessage("Please enter a Squad Number");
				return false;
			} else if (firstName == '') {
				this.showErrorMessage("Please enter a first name");
				return false;
			} else if (lastName == '') {
				this.showErrorMessage("Please enter an last name");
				return false;
			} else if (age == 0) {
				this.showErrorMessage("Please enter an age");
				return false;
			}else if (position == '') {
				this.showErrorMessage("Please enter a position");
				return false;
			}else if (nationality == '') {
				this.showErrorMessage("Please enter a nationality");
				return false;
			}else if (image == '') {
				this.showErrorMessage("Please enter an image");
				return false;
			}else {
				return true;
			}
		},
		showErrorMessage:function(message) {
			toastr.options = {
				"debug" : false,
				"positionClass" : "toast-top-center",
				"onclick" : null,
				"fadeIn" : 300,
				"fadeOut" : false,
				"progressBar" : true,
				"timeOut" : 7000,
				"closeButton" : true,
				"extendedTimeOut" : 10000,
				"preventDuplicates" : true
			}
			toastr.error(message);
		},
		showSuccessMessage:function(message) {
			toastr.options = {
				"debug" : false,
				"positionClass" : "toast-top-center",
				"onclick" : null,
				"fadeIn" : 300,
				"progressBar" : true,
				"fadeOut" : 1000,
				"timeOut" : 7000,
				"closeButton" : true,
				"extendedTimeOut" : 10000,
				"preventDuplicates" : true
			}
			toastr.success(message);
		},
		render: function(){
	 var template= _.template($('#player-details').html(), this.model.toJSON());
	 return this.$el.html(template);
  }
});
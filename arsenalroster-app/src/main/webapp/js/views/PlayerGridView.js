PlayerGridView = Backbone.View.extend({
	collection: Player,
	render: function(){
		var html1 = "<div class = 'row'>";
			 this.collection.each(function(player){
				 html1+= _.template($('#gridTemplate').html(), player.toJSON());
		});
		html1+="</div>"
		return this.$el.html(html1);
	}
})
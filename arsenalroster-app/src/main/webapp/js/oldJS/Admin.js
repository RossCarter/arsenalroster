var url = "http://localhost:8080/arsenalroster-app/arsenal/players/";

/**
 * Gets all players from database and calls "renderList" method if successful
 */
var findAll = function () {
    $.ajax({
        type: 'GET',
        url: url,
        dataType: "JSON",
        success: renderList
    });
};

/**
 * Inserts the data retrieved by findAll into each table column for the number of items retrieved
 * .DataTable() converts regular table into a datatable
 */
var renderList = function (data) {
    $.each(data, function (index, player) {
    	
        $('#playerTableBody').append('<tr><td>' + player.id 
                + '</td><td>' + player.squadNumber 
                + '</td><td>' + player.firstName
                + '</td><td>' + player.lastName 
                + '</td><td>' + player.age 
                + '</td><td>' + player.playingPosition 
                + '</td><td>' + player.nationality
                + '</td><td>' + player.goals 
                + '</td><td>' + player.assists 
                + '</td><td>' + player.appearances
                + '</td><td>' + player.image
                + '</td><td>' 
                + '<button style="float:right; min-width:110px; width:140px" class="btn btn-success editDetails" href="#" id="' + player.id + '">' + "Edit " + '<i class="fa fa-edit"></i></button>' 
                + '</td></tr>');
        }); 
        $('#playerManagementTable').DataTable(); 
        
        output='<div class = "row">';
        $.each(data, function (index, player) {
        	var image = "images/"+ player.image;
        	output+=('<div class = "col-sm-6 col-md-4 col-lg-3"><div class = "card"><img src='+'"'+image+'"'+'height="400"><p>Squad Number: ' +player.squadNumber+'</p><p>First Name: '+player.firstName+'</p><p>Last Name: ' +player.lastName+'</p></div></div>');
        	/*<p>Colour: ' + phone.colour+'</p><p>Internal Memory: ' +phone.internal_Memory+'</p><p>Operating System: ' +phone.operating_System+'</p><p>Camera: ' +phone.camera+'</p><p>Screen Size: ' +phone.screen_Size+'</p><p>Screen Type: ' +phone.screen_Type+'</p>*/
        });
        output+='</div>';
        $('#players').empty();
        $('#players').append(output);
};

/**
 * Sets the values in the modal to default so a new player can be added.
 * Shows the modal when all fields are set.
 * 
 */
var addPlayerModal = function () {
    $('#squadnumber').val("");
    $('#firstName').val("");
    $('#lastName').val("");
    $('#age').val("");
    $('#position').val("Goal Keeper");
    $('#nationality').val("");
    $('#goals').val("");
    $('#assists').val("");
    $('#appearances').val("");
    $('#image').val("");
    $('#playerRegistrationForm').modal('show');
};

/**
 * Converts the information in the fields so it can be added to the database. 
 */
var formToJSONAdd = function () {
    return JSON.stringify({
    	"squadNumber": $('#squadnumber').val(),
        "firstName": $('#firstName').val(),
        "lastName": $('#lastName').val(),
        "age": $('#age').val(),
        "playingPosition": $('#position').val(),
        "nationality": $('#nationality').val(),
        "goals": $('#goals').val(),
        "assists": $('#assists').val(),
        "appearances": $('#appearances').val(),
        "image": $('#image').val()
    });
};

/**
 * Gets the variables from the fields in the addPlayerModal.
 * Uses data from formToJSONAdd to add the data to the database.
 * Hides the addPlayerModal and alerts that a player has been added
 */
var addPlayer = function () {
	var squadNumber = $('#squadnumber').val();
	var firstName = $('#firstName').val();
	var lastName = $('#v').val();
	var age = $('#age').val();
	var position = $('#position').val();
	var nationality = $('#nationality').val();
	var goals = $('#goals').val();
	var assists = $('#assists').val();
	var appearances = $('#appearances').val();
	var image = $('#image').val();
	if(isInputFieldsForAddPlayerValid()){
			console.log("true")
		    $.ajax({
		        type: 'POST',
		        contentType: 'application/json',
		        url: url,
		        dataType: "json",
		        data: formToJSONAdd(),
		        success: function (data, textStatus, jqXHR) {		
		            $('#playerManagementTable').dataTable().fnDestroy();
		            $('#playerManagementTable tbody').empty();
		            findAll();
		            $('#playerRegistrationForm').modal('hide');
		            $("#alert_create_placeholder").hide();
		            showSuccessMessage('Player has been <strong>added</strong> to the roster!');
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		        	showErrorMessage('An error occured while adding a player!');
			        }
			    });
	}
	else{
	}
};

/**
 * Refers to the fields in the edit modal.
 * Converts the information in the fields so it can be updated in the database. 
 */
var formToJSONEdit = function () {
    return JSON.stringify({
        "id": $('#idEdit').val(),
        "squadNumber": $('#squadNumberEdit').val(),
        "firstName": $('#firstNameEdit').val(),
        "lastName": $('#lastNameEdit').val(),
        "age": $('#ageEdit').val(),
        "playingPosition": $('#editPosition').val(),
        "nationality": $('#editNationality').val(),
        "goals": $('#editGoals').val(),
        "assists": $('#editAssists').val(),
        "appearances": $('#editAppearances').val(),
        "image": $('#editImage').val()
    });
};

/**
 * Refers to the fields in the edit modal.
 * Converts the information in the fields so it can be updated in the database. 
 */
var findByIdEdit = function (id) {
    $.ajax({
        type: 'GET',
        url: url + "getByID/" + id,
        dataType: "json",
        success: function (data) {
            currentEntry = data;
            renderDetailsEdit(currentEntry);
        }
    });
};

function renderDetailsEdit(entry) {
    $('#idEdit').val(entry.id);
    $('#squadNumberEdit').val(entry.squadNumber);
    $('#firstNameEdit').val(entry.firstName);
    $('#lastNameEdit').val(entry.lastName);
    $('#ageEdit').val(entry.age);
    $('#editPosition').val(entry.playingPosition);
    $('#editNationality').val(entry.nationality);
    $('#editGoals').val(entry.goals);
    $('#editAssists').val(entry.assists);
    $('#editAppearances').val(entry.appearances);
    $('#editImage').val(entry.image);
    document.getElementById("myEditLabel").innerHTML = "Edit Player Details:";
    $('#editPlayerModal').modal('show');
}

var updatePlayer = function (id) {
    $.ajax({
        type: 'PUT',
        contentType: 'application/json',
        url: url + $('#idEdit').val(),
        dataType: "json",
        data: formToJSONEdit(),
        success: function (data, textStatus, jqXHR) {
            $('#playerManagementTable').dataTable().fnDestroy();
            $('#playerManagementTable tbody').empty();
            findAll();
            $('#editPlayerModal').modal('hide');
            $("#alert_edit_placeholder").hide();
            showSuccessMessage('Player details have been <strong>updated!</strong>');
        },
        error: function (jqXHR, textStatus, errorThrown) {
        	showErrorMessage('An error occured while updating player details!');
        }
    });
   
};

var addPlayerThroughEditModal = function () {
	$('#idEdit').val("");
	$('#squadNumberEdit').val("");
	$('#firstNameEdit').val("");
	$('#lastNameEdit').val("");
	$('#ageEdit').val("");
	$('#editPosition').val("Goal Keeper");
	$('#editNationality').val("");
	$('#editGoals').val("");
	$('#editAssists').val("");
	$('#editAppearances').val("");
	$('#editImage').val("");
	document.getElementById("myEditLabel").innerHTML = "Register Player:";
};

var formToJSONAddThroughEdit = function () {
    return JSON.stringify({
    	"squadNumber": $('#squadNumberEdit').val(),
        "firstName": $('#firstNameEdit').val(),
        "lastName": $('#lastNameEdit').val(),
        "age": $('#ageEdit').val(),
        "playingPosition": $('#editPosition').val(),
        "nationality": $('#editNationality').val(),
        "goals": $('#editGoals').val(),
        "assists": $('#editAssists').val(),
        "appearances": $('#editAppearances').val(),
        "image": $('#editImage').val()
    });
};

var addPlayerEdit = function () {
	var squadNumber = $('#squadNumberEdit').val();
	var firstName = $('#firstNameEdit').val();
	var lastName = $('#lastNameEdit').val();
	var age = $('#ageEdit').val();
	var position = $('#editPosition').val();
	var nationality = $('#editNationality').val();
	var goals = $('#editGoals').val();
	var assists = $('#editAssists').val();
	var appearances = $('#editAppearances').val();
	var image = $('#editImage').val();
	if(isInputFieldsForEditPlayerValid()){			
		    $.ajax({
		        type: 'POST',
		        contentType: 'application/json',
		        url: url,
		        dataType: "json",
		        data: formToJSONAddThroughEdit(),
		        success: function (data, textStatus, jqXHR) {		
		            $('#playerManagementTable').dataTable().fnDestroy();
		            $('#playerManagementTable tbody').empty();
		            findAll();
		            $('#editPlayerModal').modal('hide');
		            $("#alert_edit_placeholder").hide();
		            showSuccessMessage('Player has been <strong>added</strong> to the roster!');
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		        	showErrorMessage('An error occured while adding a player!');
			        }
			    });
	}
	else{
		
	}
};

var deletePlayer = function (id) {
    $.ajax({
        type: 'DELETE',
        url: url + $('#idEdit').val(),
        success: function (data, textStatus, jqXHR) {
            $('#playerManagementTable').dataTable().fnDestroy();
            $('#playerManagementTable tbody').empty();
            findAll();
            $('#editPlayerModal').modal('hide');
            showSuccessMessage('Player has been <strong>removed</strong> from the roster!');
        },
        error: function (jqXHR, textStatus, errorThrown) {
        	showErrorMessage('An error occured while removing a player!');
        }

    });
};

var isInputFieldsForAddPlayerValid = function() {
	var squadNumber = $('#squadNumber').val();
	var firstName = $('#firstName').val();
	var lastName = $('#lastName').val();
	var age = $('#age').val();
	var position = $('#Position').val();
	var nationality = $('#Nationality').val();
	var goals = $('#Goals').val();
	var assists = $('#Assists').val();
	var appearances = $('#Appearances').val();
	var image = $('#Image').val();
	
	console.log(squadNumber);
	if (squadNumber == '') {
		showErrorMessage("Please enter a Squad Number");
		return false;
	} else if (firstName == '') {
		showErrorMessage("Please enter a first name");
		return false;
	} else if (lastName == '') {
		showErrorMessage("Please enter an last name");
		return false;
	} else if (age == 0) {
		showErrorMessage("Please enter an age");
		return false;
	}else if (position == '') {
		showErrorMessage("Please enter a position");
		return false;
	}else if (nationality == '') {
		showErrorMessage("Please enter a nationality");
		return false;
	}else if (image == '') {
		showErrorMessage("Please enter an image");
		return false;
	}else {
		return true;
	}
}

var isInputFieldsForEditPlayerValid = function() {
	var squadNumber = $('#squadNumberEdit').val();
	var firstName = $('#firstNameEdit').val();
	var lastName = $('#lastNameEdit').val();
	var age = $('#ageEdit').val();
	var position = $('#editPosition').val();
	var nationality = $('#editNationality').val();
	var goals = $('#editGoals').val();
	var assists = $('#editAssists').val();
	var appearances = $('#editAppearances').val();
	var image = $('#editImage').val();
	
	console.log(squadNumber);
	if (squadNumber == '') {
		showErrorMessage("Please enter a Squad Number");
		return false;
	} else if (firstName == '') {
		showErrorMessage("Please enter a first name");
		return false;
	} else if (lastName == '') {
		showErrorMessage("Please enter an last name");
		return false;
	} else if (age == 0) {
		showErrorMessage("Please enter an age");
		return false;
	}else if (position == '') {
		showErrorMessage("Please enter a position");
		return false;
	}else if (nationality == '') {
		showErrorMessage("Please enter a nationality");
		return false;
	}else if (image == '') {
		showErrorMessage("Please enter an image");
		return false;
	}else {
		return true;
	}
}

$(document).ready(function(){

});

//var showErrorMessage = function(message) {
//	toastr.options = {
//		"debug" : false,
//		"positionClass" : "toast-top-center",
//		"onclick" : null,
//		"fadeIn" : 300,
//		"fadeOut" : false,
//		"progressBar" : true,
//		"timeOut" : 7000,
//		"closeButton" : true,
//		"extendedTimeOut" : 10000,
//		"preventDuplicates" : true
//	}
//	toastr.error(message);
//};
//
//var showSuccessMessage = function(message) {
//	toastr.options = {
//		"debug" : false,
//		"positionClass" : "toast-top-center",
//		"onclick" : null,
//		"fadeIn" : 300,
//		"progressBar" : true,
//		"fadeOut" : 1000,
//		"timeOut" : 7000,
//		"closeButton" : true,
//		"extendedTimeOut" : 10000,
//		"preventDuplicates" : true
//	}
//	toastr.success(message);
//};

package com.arsenalroster.arsenalroster_app.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.arsenalroster.arsenalroster_app.rest.JaxRsActivator;


public class JaxRsActivatorTest {

	/**
	 * Test the instance of JaxRsActivator was created successfully
	 */
	@Test
	public void testRestApplicationInstanceSuccess() {
		final JaxRsActivator jaxRs = new JaxRsActivator();
		assertTrue(jaxRs instanceof JaxRsActivator);
	}

}
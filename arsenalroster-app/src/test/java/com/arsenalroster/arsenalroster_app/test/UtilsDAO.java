package com.arsenalroster.arsenalroster_app.test;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
@LocalBean
public class UtilsDAO {

    @PersistenceContext
    private EntityManager em;
    
	public void deleteTable(){
		em.createQuery("DELETE FROM Player").executeUpdate();
		em.createNativeQuery("ALTER TABLE Player AUTO_INCREMENT=1")
		.executeUpdate();
		
	}
      
}

package com.arsenalroster.arsenalroster_app.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.arsenalroster.arsenalroster_app.model.Player;

public class PlayerTest {

	@Test
	public void testGettersAndSetters() {
		Player player1 = new Player();
		player1.setID(1);
		player1.setFirstName("Mesut");
		player1.setLastName("Ozil");
		player1.setAge(29);
		player1.setNationality("German");
		player1.setPlayingPosition("Midfield");
		player1.setSquadNumber(11);
		player1.setGoals(20);
		player1.setAssists(4);
		player1.setAppearances(12);
		
		assertEquals(player1.getId(),1);
		assertEquals(player1.getFirstName(),"Mesut");
		assertEquals(player1.getLastName(),"Ozil");
		assertEquals(player1.getAge(),29);
		assertEquals(player1.getNationality(),"German");
		assertEquals(player1.getPlayingPosition(),"Midfield");
		assertEquals(player1.getSquadNumber(),11);
		assertEquals(player1.getGoals(),20);
		assertEquals(player1.getAssists(),4);
		assertEquals(player1.getAppearances(),12);
	}

}

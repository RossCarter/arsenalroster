package com.arsenalroster.arsenalroster_app.test;

import static org.junit.Assert.*;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.arsenalroster.arsenalroster_app.data.PlayerDAO;
import com.arsenalroster.arsenalroster_app.model.Player;
import com.arsenalroster.arsenalroster_app.rest.JaxRsActivator;
import com.arsenalroster.arsenalroster_app.rest.PlayerWS;
@RunWith(Arquillian.class)
public class PlayerWSTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(PlayerDAO.class, Player.class,
						JaxRsActivator.class,PlayerWS.class,
						UtilsDAO.class)
			//	.addPackage(EventCause.class.getPackage())
			//	.addPackage(EventCauseDAO.class.getPackage())
						//this line will pick up the production db
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	 
	@EJB
	private PlayerWS playerWS;
	
	@EJB
	private PlayerDAO playerDAO;
	
	@EJB
	private UtilsDAO utilsDAO;
	 
	@Before
	public void setUp() {
		utilsDAO.deleteTable();
		Player player1 = new Player();
		player1.setFirstName("Mesut");
		player1.setLastName("Ozil");
		player1.setAge(29);
		player1.setNationality("German");
		player1.setPlayingPosition("Midfield");
		player1.setSquadNumber(11);
		player1.setGoals(20);
		player1.setAssists(4);
		player1.setAppearances(12);
		playerDAO.addPlayer(player1);
		
		Player player2 = new Player();
		player2.setFirstName("Henrikh");
		player2.setLastName("Mkhitaryan");
		player2.setAge(29);
		player2.setNationality("Armenian");
		player2.setPlayingPosition("Forward");
		player2.setSquadNumber(7);
		player2.setGoals(25);
		player2.setAssists(45);
		player2.setAppearances(88);
		playerDAO.addPlayer(player2);
		
	}
	
	@Test
	public void testGetAllPlayers() {
		final Response response = playerWS.getAllPlayers();
		List<Player> playerList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(playerList.size(), 2);
		Player player1 = playerList.get(0);
		assertEquals("Mesut",player1.getFirstName());
		Player player2 = playerList.get(1);
		assertEquals("Henrikh",player2.getFirstName());
		assertEquals(player2.getId(),2);
	}
	
	@Test
	public void testGetPlayersByNationality() {
		final Response response = playerWS.getPlayersByNationality("German");
		List<Player> playerList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(playerList.size(), 1);
		Player player1 = playerList.get(0);
		assertEquals("Mesut",player1.getFirstName());
	}
	
	@Test
	public void testGetPlayersByPosition() {
		final Response response = playerWS.getPlayersByPosition("Forward");
		List<Player> playerList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(playerList.size(), 1);
		Player player1 = playerList.get(0);
		assertEquals("Henrikh",player1.getFirstName());
	}
	
	@Test
	public void testGetPlayerByID() {
		final Response response = playerWS.getPlayerByID(2);
		Player player =(Player) response.getEntity();
		assertEquals("Henrikh",player.getFirstName());
	}
	
	@Test
	public void testGetPlayerByFirstName() {
		Player player = new Player();
		player.setFirstName("Henrikh");
		player.setLastName("Cech");
		player.setAge(39);
		player.setNationality("Czech");
		player.setPlayingPosition("Goalkeeper");
		player.setSquadNumber(1);
		player.setGoals(0);
		player.setAssists(0);
		player.setAppearances(100);
		Response response = playerWS.addPlayer(player);
		assertEquals(HttpStatus.SC_CREATED, response.getStatus());
		
		response = playerWS.getPlayerByFirstName("Mesut");
		List<Player> players =(List<Player>) response.getEntity();
		assertEquals("Mesut",players.get(0).getFirstName());
		assertEquals(1,players.size());
		response = playerWS.getPlayerByFirstName("Henrikh");
		players =(List<Player>) response.getEntity();
		assertEquals("Henrikh",players.get(0).getFirstName());
		assertEquals("Henrikh",players.get(1).getFirstName());
		assertEquals(2,players.size());
	}
	
	@Test
	public void testGetPlayerByLastName() {
		Player player = new Player();
		player.setFirstName("Peter");
		player.setLastName("Mkhitaryan");
		player.setAge(39);
		player.setNationality("Czech");
		player.setPlayingPosition("Goalkeeper");
		player.setSquadNumber(1);
		player.setGoals(0);
		player.setAssists(0);
		player.setAppearances(100);
		Response response = playerWS.addPlayer(player);
		assertEquals(HttpStatus.SC_CREATED, response.getStatus());
		response = playerWS.getPlayerByLastName("Ozil");
		List<Player> players =(List<Player>) response.getEntity();
		assertEquals("Ozil",players.get(0).getLastName());
		assertEquals(1,players.size());
		response = playerWS.getPlayerByLastName("Mkhitaryan");
		players =(List<Player>) response.getEntity();
		assertEquals("Mkhitaryan",players.get(0).getLastName());
		assertEquals("Mkhitaryan",players.get(1).getLastName());
		assertEquals(2,players.size());
	}
	
	@Test
	public void testAddPlayer() {
		Player player = new Player();
		player.setFirstName("Petr");
		player.setLastName("Cech");
		player.setAge(39);
		player.setNationality("Czech");
		player.setPlayingPosition("Goalkeeper");
		player.setSquadNumber(1);
		player.setGoals(0);
		player.setAssists(0);
		player.setAppearances(100);
		Response response = playerWS.addPlayer(player);
		assertEquals(HttpStatus.SC_CREATED, response.getStatus());
		player = (Player) response.getEntity();
		assertEquals(player.getFirstName(),"Petr");
		assertEquals(player.getLastName(),"Cech");
		assertEquals(player.getAge(),39);
		assertEquals(player.getNationality(),"Czech");
		assertEquals(player.getPlayingPosition(),"Goalkeeper");
		assertEquals(player.getSquadNumber(),1);
		assertEquals(player.getGoals(),0);
		assertEquals(player.getAssists(),0);
		assertEquals(player.getAppearances(),100);
	}
	
	@Test
	public void testDeletePlayer() {
		Response response = playerWS.getAllPlayers();
		List<Player> playerList = (List<Player>) response.getEntity();
		assertEquals(playerList.size(),2);
		playerWS.deletePlayer(1);
		response = playerWS.getAllPlayers();
		playerList = (List<Player>) response.getEntity();
		assertEquals(playerList.size(),1);
		response = playerWS.getPlayerByID(1);
		final Player player = (Player) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(null, player);
	}
	
	@Test
	public void testUpdateUser() {
		Response response = playerWS.getPlayerByID(1);
		Player player = (Player) response.getEntity();
		player.setAge(99);
		response = playerWS.updatePlayer(player);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		player = (Player) response.getEntity();
		assertEquals(player.getAge(),99);
	}
	
	@Test
	public void testGetPlayersByGoals() {
		Response response = playerWS.getPlayersByGoals(4);
		List<Player> playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(2,playersList.size());
		
		
		response = playerWS.getPlayersByGoals(800);
		playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(0,playersList.size());
		
		response = playerWS.getPlayersByGoals(23);
		playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(1,playersList.size());
		assertEquals("Henrikh",playersList.get(0).getFirstName());
	}
	
	@Test
	public void testGetPlayersByAppearances() {
		Response response = playerWS.getPlayersByAppearances(4);
		List<Player> playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(2,playersList.size());
		
		
		response = playerWS.getPlayersByAppearances(8000);
		playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(0,playersList.size());
		
		response = playerWS.getPlayersByAppearances(15);
		playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(1,playersList.size());
		assertEquals("Henrikh",playersList.get(0).getFirstName());
	}
	
	@Test
	public void testGetPlayerBySquadNumber() {
		Response response = playerWS.getPlayerBySquadNumber(7);
		List<Player> playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(1,playersList.size());
		assertEquals("Henrikh",playersList.get(0).getFirstName());
		
		
		response = playerWS.getPlayerBySquadNumber(99998);
		playersList = (List<Player>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(0,playersList.size());
	}

}
